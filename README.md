# wis-theme

A theme that let **Waybar** Imitates **Swaybar**.
Clone the project and copy files to `~/.config/waybar/`.Restart and enjoy.

# Screenshot
![Look at the top.](https://codeberg.org/WodeShengli/wis-theme/raw/commit/4c523755d6af0f397760eb46e3568ee9a6efb651/screenshot.png  "wis-theme")
